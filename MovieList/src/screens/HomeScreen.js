import React, {useEffect} from 'react';
import {Image, View, StyleSheet, Text, FlatList, Modal,
    ActivityIndicator} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import {getMovies} from '../actions';
import { Rating, AirbnbRating } from 'react-native-ratings';

const HomeScreen = props => {
  useEffect(() => {
    props.getMovies();
  }, []);

  if(props.movies.length == 0){
    return (
        <Modal
          transparent={true}
          animationType={'none'}
          visible={props.loading}
          onRequestClose={() => {console.log('close modal')}}>
          <View style={styles.modalBackground}>
            <View style={styles.activityIndicatorWrapper}>
              <ActivityIndicator
                animating={props.loading} />
            </View>
          </View>
        </Modal>
      )
  }

  return (
    <View style={styles.containerStyle}>
    <Text style={styles.headerTextStyle}>Movie Turbo</Text>
      <FlatList
        data={props.movies}
        renderItem={({item}) => {
          return (
            <View style={styles.itemStyle}>
              <Image
                style={styles.image}
                source={{uri: item.medium_cover_image}}
              />
              <View style={styles.itemTextContainerStyle}>
                <Text style={styles.itemTitleStyle} numberOfLines={2} ellipsizeMode='tail'>{item.title}</Text>
                <AirbnbRating
  count={5}
  defaultRating={item.rating/2}
  showRating = {false}
  isDisabled = {true}
  size={20}
  ratingContainerStyle={styles.ratingStyle}
  starContainerStyle={styles.ratingStyle}
/>
                <Text style={styles.itemGenreStyle}>{item.genres.toString().replaceAll(","," | ")}</Text>
              </View>
            </View>
          );
        }}
        keyExtractor={item => item.id.toString()}
      />
    </View>
  );
};

const mapStateToProps = state => {
  return {
    movies: state.fetchMovie.movies,
    loading: state.fetchMovie.loading,
  };
};

const styles = StyleSheet.create({
    headerTextStyle: {
        alignSelf: 'center',
        fontSize: 25,
    color: '#4895e7',
    padding:10,
      },

  containerStyle: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  itemStyle: {
    flex: 1,
    flexDirection: 'row',
    margin: 10,
  },
  itemTextContainerStyle: {
    margin: 20,
  },

  logo: {
    height: 100,
    width: 150,
    marginTop: 100,
  },
  container: {
    height: 300,
    margin: 10,
    backgroundColor: '#FFF',
    borderRadius: 6,
  },
  image: {
    height: 200,
    width: 150,
    borderRadius: 4,
  },
  itemTitleStyle: {
      width:200,
    fontSize: 20,
    fontWeight:'400',
    color: '#000000',
  },
  ratingStyle:{
      marginTop:20,
      alignContent:'flex-start',
      alignItems:'flex-start',
  },
  itemGenreStyle: {
    fontSize: 15,
    color: '#4895e7',
    marginTop:30,
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});

export default connect(mapStateToProps, {
  getMovies,
})(HomeScreen);
