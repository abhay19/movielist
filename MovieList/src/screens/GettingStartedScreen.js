import React from 'react';
import {Image, View, StyleSheet, Text} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const GettingStartedScreen = props => {
  return (
    <View style={styles.containerStyle}>
      
        <View style={styles.headerViewStyle}>
        <Image
        style={styles.logo}
        source={require('../assets/logo.jpg')}
        />
        </View>
      <Text style = {styles.textStyle}>WELCOME</Text>
      <TouchableOpacity style={styles.buttonStyle} onPress={() => {
props.navigation.reset({
            index: 0,
            routes: [
              {
                name: 'Login',
              },
            ],
          })
      }}>
          <Text style={styles.buttonTextStyle}>GETTING START</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
      alignSelf:'center',
      justifyContent: 'center',
    backgroundColor:'#4895e7',
    borderRadius: 35,
    height: 70,
    width:250,
    marginTop:200,
  },
  textStyle: {
    fontSize: 30,
    fontWeight:'500',
    alignSelf: 'center',
    alignContent: 'center',
    color: '#000000',
    marginTop:100,
  },
  buttonTextStyle: {
    fontSize: 20,
    alignSelf:'center',
    alignContent:'center',
    color: '#ffffff',
  },
  headerViewStyle: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  containerStyle: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  cardStyle: {
    width: '100%',
    marginEnd: 10,
    height: '10%',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  logo: {
    height: 100,
    width: 150,
    marginTop: 100,
  },
});

export default GettingStartedScreen;
