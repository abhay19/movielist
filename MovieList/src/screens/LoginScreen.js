import React from 'react';
import {Image, View, StyleSheet, Text, Alert} from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import {usernameTextChanged,passwordTextChanged} from '../actions';
import {connect} from 'react-redux';
const LoginScreen = props => {
  return (
    <View style={styles.containerStyle}>
      
        <View style={styles.headerViewStyle}>
        <Image
        style={styles.logo}
        source={require('../assets/logo.jpg')}
        />
        </View>
        <View style={styles.inputStyle}>
        <TextInput
      style={styles.textInputStyle}
      onChangeText={text => props.usernameTextChanged(text)}
        value={props.username}
	  placeholder="User name"
    />
    </View>
    <View style={styles.inputStyle}>
     <TextInput
     secureTextEntry={true}
      style={styles.textInputStyle}
      onChangeText={text => props.passwordTextChanged(text)}
        value={props.password}
	  placeholder="Password"
    />
    </View>
      <TouchableOpacity style={styles.buttonStyle} onPress={()=>{
          if(props.username == '' || props.password == ''){
            Alert.alert(
      "Alert",
      "Please enter all details",
    );
    return;
          }
          props.navigation.reset({
            index: 0,
            routes: [
              {
                name: 'Home',
              },
            ],
          })
      }}>
          <Text style={styles.buttonTextStyle}>LOGIN</Text>
      </TouchableOpacity>
    </View>
  );
};

const mapStateToProps = state => {
    return {
      username: state.login.username,
      password:state.login.password,
      loading: state.login.loading,
    };
  };
  

const styles = StyleSheet.create({
  buttonStyle: {
      alignSelf:'center',
      alignContent: 'center',
      justifyContent:'center',
    backgroundColor:'#4895e7',
    borderRadius: 30,
    height: 60,
    width:200,
    marginTop:60,
  },
  inputStyle: {
    marginHorizontal: 50,
    marginTop:20,
  },
  textInputStyle:{ 
    height: 50, 
    fontSize: 20,
    placeholderTextColor: 'gray',
},
  textStyle: {
    fontSize: 30,
    alignSelf: 'center',
    alignContent: 'center',
    color: '#000000',
    marginTop:100,
  },
  buttonTextStyle: {
    fontSize: 20,
    alignSelf:'center',
    alignContent:'center',
    color: '#ffffff',
  },
  headerViewStyle: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  containerStyle: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  cardStyle: {
    width: '100%',
    marginEnd: 10,
    height: '10%',
    borderRadius: 5,
    backgroundColor: 'white',
  },
  logo: {
    height: 100,
    width: 150,
    marginTop: 100,
  },
});

export default connect(mapStateToProps, {
    usernameTextChanged,
    passwordTextChanged,
  })(LoginScreen);
