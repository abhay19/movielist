import React from 'react';
import {Image, View, StyleSheet} from 'react-native';

const SplashScreen = props => {
  return (
    <View style={styles.containerStyle}>
      
        <View style={styles.headerViewStyle}>
        <Image
        style={styles.logo}
        source={require('../assets/logo.jpg')}
    />
        </View>
      
    </View>
  );
};

const styles = StyleSheet.create({
  headerTextStyle: {
    fontSize: 50,
    alignSelf: 'center',
    color: '#355667',
  },
  headerViewStyle: {
    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  containerStyle: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  logo: {
    height: 100,
    width: 150,
  },
});

export default SplashScreen;
