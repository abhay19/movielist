export const FETCH_MOVIES = 'fetch_movies';
export const LOADING = 'loading';
export const USERNAME_CHANGE = 'username_change';
export const PASSWORD_CHANGE = 'password_change';