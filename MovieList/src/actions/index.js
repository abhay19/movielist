import axios from 'axios';
import {FETCH_MOVIES, PASSWORD_CHANGE, USERNAME_CHANGE, LOADING} from './types';
const endPoint = 'https://yts.mx/api/v2/list_movies.json';

export const getMovies = () => {
  return dispatch => {
    dispatch({
      type: LOADING,
      payload: true,
    });
    
      axios
        .get(endPoint)
        .then(response => {
          console.log(response);
          if (!response.data.data.movies || response.data.data.movies.length == 0) {
        
            dispatch({
              type: LOADING,
              payload: false,
            });
            return;
          }
          dispatch({
            type: FETCH_MOVIES,
            payload: response.data.data.movies,
          });
          dispatch({
            type: LOADING,
            payload: false,
          });
        })
        .catch(error => {
          console.log(error);
        });
    
  };
};


export const usernameTextChanged = text => {
    return {
      type: USERNAME_CHANGE,
      payload: text,
    };
  };

  export const passwordTextChanged = text => {
    return {
      type: PASSWORD_CHANGE,
      payload: text,
    };
  };