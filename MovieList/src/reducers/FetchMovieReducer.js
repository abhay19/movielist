import {
  LOADING,
  FETCH_MOVIES,
} from '../actions/types';

const INITIAL_STATE = {
  movies: [],
  loading: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_MOVIES: {
      return {...state, movies: action.payload};
    }
    case LOADING: {
      return {...state, loading: action.payload};
    }
    default:
      return INITIAL_STATE;
  }
};
