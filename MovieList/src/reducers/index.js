import {combineReducers} from 'redux';
import FetchMovieReducer from './FetchMovieReducer';
import LoginReducer from './LoginReducer';


export default combineReducers({
  fetchMovie: FetchMovieReducer,
  login: LoginReducer,
});
