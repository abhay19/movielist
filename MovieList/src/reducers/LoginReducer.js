import {
    LOADING,
    USERNAME_CHANGE,
    PASSWORD_CHANGE
  } from '../actions/types';
  
  const INITIAL_STATE = {
    username: '',
    password: '',
  };
  
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case USERNAME_CHANGE: {
        return {...state, username: action.payload};
      }
      case PASSWORD_CHANGE: {
        return {...state, password: action.payload};
      }
      case LOADING: {
        return {...state, loading: action.payload};
      }
      default:
        return INITIAL_STATE;
    }
  };
  